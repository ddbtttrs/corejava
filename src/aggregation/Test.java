package aggregation;

public class Test {

	public static void main(String[] args) {
		Car c = new Car();
		
		c.setColor("Blue");
		c.setCompany("Tesla");
		c.setPrice(50000);
		
		Employee e = new Employee(); 
		
		e.setId(531);
		e.setName("Sartha Bista");
		e.setSalary(3749328);
		e.setCar(c);
		
		
		System.out.println("ID = "+e.getId());
		System.out.println("ID = "+e.getName());
		System.out.println("ID = "+e.getSalary());
		System.out.println("--------- Car Info -------");
		System.out.println("color = "+e.getCar().getColor());
		System.out.println("color = "+e.getCar().getCompany());
		System.out.println("color = "+e.getCar().getPrice());
		
		
	}
}
