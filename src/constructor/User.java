package constructor;

public class User {
	// instance variable
	String userName;
	String password;
	
	//-----------default constructor
//	User(){
//		userName = "root";
//		password = "124";
//	}
	
	//--------------Parameterized constructor
	
	User(String userName, String password){
		this.userName = userName;
		this.password = password;
	}
	
	User(String userName){
		this.userName = userName;
		
	}
	
	void printUser() {
		System.out.println("UserName = "+userName);
		System.out.println("Password = "+password);
	
	}
	
	public static void main(String[] args) {
		 
		User u = new User("ram", "098098");		
	}
		
}
