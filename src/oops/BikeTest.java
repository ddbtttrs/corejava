package oops;

import java.util.Scanner;

public class BikeTest {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Bike bike1 = new Bike();
		Bike bike2 = new Bike();
		
		System.out.println("enter cadance value: ");
		bike1.changeCadence(sc.nextInt());
		
		System.out.println("enter gear value (1-7): ");
		bike1.changeGear(sc.nextInt());
		System.out.println("enter speedUp value: ");
		bike1.increaseSpeed(sc.nextInt());
		System.out.println("enter speedDown value: ");
		bike1.decereseSpeed(sc.nextInt());
		bike1.printStatus();
		
	}

}