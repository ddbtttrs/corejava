package oops;

public class Student {
	String name;
	int roll_no;
	int phone_num;
	String add;
	
	void nameAssign(String in_name){
		name = in_name;
	}
	
	void phoneAssign(int num){
		phone_num = num;
	}
	
	void rollAssign(int roll){
		roll_no = roll;
	}
	
	void addAssign(String in_add){
		add = in_add;
	}

	void printDetail() {
		System.out.println("Name:" + name);
		System.out.println("Roll:" + roll_no);
		System.out.println("Add:" + add);
		System.out.println("Phone:" + phone_num);
	}
}
