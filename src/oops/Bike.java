package oops;

public class Bike {
	
	int gear = 1;
	int speed = 0;
	int cadance = 0;
	
	void increaseSpeed(int inc){
		speed += inc;
	};
	
	void decereseSpeed(int dec) {
		if(!(dec>speed)) {
			speed -= dec;
		} else {
			System.out.println("Enter value less than current speed: ");
		}
	};
	
	void changeGear(int gear_shift) {
		if(gear_shift>0 && gear_shift<7) {
			gear = gear_shift;
		} else {
			System.out.println("Enter value between 0-7: ");
		}
	};
	
	void changeCadence(int change) {
		cadance = change;
	}
	
	void printStatus() {
		System.out.println("cadance: "+ cadance);
		System.out.println("speed: "+ speed);
		System.out.println("gear: "+ gear);
	}
	
	
	
	
	
}
