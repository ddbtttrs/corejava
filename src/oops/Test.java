package oops;

import java.util.Scanner;

public class Test {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		//Math book
		
		Book b = new Book();
		System.out.println("Enter Title: ");
		b.name = sc.next();
		
		System.out.println("Enter Author: ");
		b.author = sc.next();
		
		System.out.println("Enter Price: ");
		b.price = sc.nextInt();
		
		System.out.println("Enter Page: ");
		b.pages = sc.nextInt();
		
		b.printBookInfo();
		
		Book c = new Book();
		System.out.println("Enter Title: ");
		c.name = sc.next();
		
		System.out.println("Enter Author: ");
		c.author = sc.next();
		
		System.out.println("Enter Price: ");
		c.price = sc.nextInt();
		
		System.out.println("Enter Page: ");
		c.pages = sc.nextInt();
		c.printBookInfo();
	}
}
