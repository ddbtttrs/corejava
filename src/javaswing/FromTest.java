package javaswing;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class FromTest {

	public static void main(String[] args) {
		
		JFrame form = new JFrame();
		
		form.setSize(600, 500);
		form.setTitle("Test Form");
		form.setLayout(null);
		
		// create JLable
		JLabel jl = new JLabel("WELCOME");
		jl.setBounds(120, 100, 130, 20);		
		form.add(jl);	
		
		
		form.setVisible(true);
	}
	
}
