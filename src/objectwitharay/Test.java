package objectwitharay;

import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		
		//Store and print data of 5 students
		
		// create array
		Student students[] = new Student [5];
		
		
		// write data in array
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < 5; i++) {
			Student s = new Student();
			System.out.println("Write Fname: ");
			s.setFname(sc.next());
			System.out.println("Write Lname: ");
			s.setLname(sc.next());
			System.out.println("Write Age: ");
			s.setAge(sc.nextInt());
			System.out.println("Write City: ");
			s.setCity(sc.next());
			System.out.println("Write Collage: ");
			s.setCollage(sc.next());
			
			students[i] = s;
		}
		
		
		
		// read from array
		for(Student s : students) {
			System.out.println(s);
		}
		
	}
}
