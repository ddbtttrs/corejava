package constructor_assignment;

public class TriangleParam {
	
	int l ;
	int b;
	int h;

	TriangleParam(int len, int breth, int hypo){
		l = len;
		b = breth;
		h = hypo;
	}
	
	void getAreaPerimeter(){
		
		int a;
		int p;
		
		a = (l*b)/2;
		System.out.println("Area of Triangle: " + a);
		
		p = l + b + h;
		System.out.println("Perimeter of Triangle: " + p);
	}
	
}
