package constructor_assignment;

public class Employee {
	
	int salary;
	
	int hours_worked;
	
	void getInfo(int salary, int hr_w) {
		this.salary = salary;
		hours_worked = hr_w;
	}
	
	void AddSal() {
		if(salary < 500) {
			salary += 10;
			System.out.println("Salary < 500: "+ salary);
		} else {
			System.out.println("Salary: "+ salary);
		}
	}
	
	void AddWork() {
		if(hours_worked > 6) {
			salary += 5;
			System.out.println("Salary hr > 6: "+ salary);
		} else {
			System.out.println("Salary: "+ salary);
		}
	}
	
}
