package constructor_assignment;

public class RectangleTest {

	public static void main(String[] args) {
		
		Rectangle r = new Rectangle(4, 5);
		Rectangle r1 = new Rectangle(5, 8);
		
		r.Area();
		r1.Area();
	}
}
