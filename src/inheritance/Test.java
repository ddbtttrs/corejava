package inheritance;

public class Test {

	public static void main(String[] args) {
		Programmer p = new Programmer();
		
		p.name = "Sarthak Bista";
		p.age = 25;
		p.company = "Microsoft";
		p.salary = 143000;
		p.bonus = 25000;
		p.progLang =  "Java";
		p.project = "G-Earth";
		
		p.print();
	}
	
	
}
