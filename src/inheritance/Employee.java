package inheritance;

public class Employee {
	
	String name;
	int age;
	String company;
	int salary;
	
	void print (){
		System.out.println("Name: " + name);
		System.out.println("age: " + age);
		System.out.println("Company: " + company);
		System.out.println("Salary: " + salary);
	}
}
