package methodoverriding;

public class Test {

	public static void main(String[] args) {
		
		PrabhuBank pb = new PrabhuBank();
		
		pb.getBankName();
		pb.getInterestRate();
		
		CentralBank bank = new PrabhuBank();
		
	}
	
	void printBankInfo(CentralBank b) {
		b.getBankName();
		b.getInterestRate();
	}
	
}
