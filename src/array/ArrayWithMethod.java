package array;

import java.util.Arrays;

public class ArrayWithMethod {
	

	public static void main(String[] args) {		
		
		//print sum of array
		int data [] = {5,5,5,5};
		sum(data);
		
		//print array of city 
		String city[] = {"Kathmandu", "Boston", "Budapest", "Tumbuktu"};
		printCity(city);
		
		//print array of contries
		String[] result= getCountries();
		System.out.println("your contries: " + Arrays.toString(result));
		
		//print odd numbers
		int [] oddnums = getOddNumber();
		System.out.println("odd numbers are: " + Arrays.toString(oddnums));
		
		
	};
		
	//calculate sum o array
	static void sum(int values []) {
		int sum = 0;
		for(int x : values) {
		sum += x;				
		}
		System.out.println("sum: " + sum);
	};
	
	// WAP to pass and print array of 5 cities using method;	
	static void printCity(String city[]) {
		
		System.out.println("Cities are: "+ Arrays.toString(city));
	};
	
	//print contries
	static String[] getCountries() {
		
		String country [] = {"Nepal", "USA", "UK", "Turky"};
		return country;
		
	};
	
// WAP to generate aray of input = 1 - 100 and return odd numbers 
	static int[] getOddNumber() {
		
		int number [] = new int[50];
		
		int j = 0; 
		
		for(int i = 1; i <= 100; i++) {
			if (i%2 !=0) {
				number[j] = i;
				j++;
			}			
		}
		return number;
	}
	
	
}




