package array;

import java.util.Scanner;

public class ArrayDocs {
	
	public static void main(String[] args) {
	
		//WAP to store and pront age of  5 student
		//create array
		// write data in array
		//read data from array
		
		int age[] = new int[5];
		
		Scanner sc = new Scanner(System.in);
		
		// write array
		for(int i = 0; i< age.length; i++) {
			
			System.out.println("Enter your value: ");
			age [i] = sc.nextInt();
		}
		
		//read array
		int s = 0;
		for(int x : age) {
			x += x;
			System.out.println(x);
		}
}
	
	
	
	
}
