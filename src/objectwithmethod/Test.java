package objectwithmethod;

public class Test {

	public static void main(String[] args) {
		
		Product p = new Product();
		Test t = new Test();
		
		
//		p.setId(123);
//		p.setName("Sarthak");
//		p.setPrice(989);
//		p.setCompany("Gucci");		
		
		Product pd = t.getProductData();
		t.printProduct(pd);
		
	}
		
	
	// ------------ object as paramater --------------------------
	void printProduct (Product p) {
		
		System.out.println("Id = "+p.getId());
		System.out.println("Name = "+p.getName());
		System.out.println("Price = "+p.getPrice());
		System.out.println("Company = "+p.getCompany());
	}
	
	// ----------------------object as return type ----------------
	
	Product  getProductData () {
		Product p = new Product();
		
		p.setId(123);
		p.setName("Sarthak");
		p.setPrice(989);
		p.setCompany("Gucci");
		
		return p;
	}
	
}
