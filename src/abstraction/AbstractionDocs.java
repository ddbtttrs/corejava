package abstraction;

public class AbstractionDocs {

	/*
	 * 
	 * --------------Abstraction----------------------
	 * 
	 * # Process of hiding implimentation details in the program 
	 * 	 is known as abstraction.
	 * 
	 * # To achieve abstraction:
	 * 		
	 * 		a> Abstract class:
	 * 			# Class which contains abstract method and non-abstract
	 * 			  is known as abstract class.
	 * 
	 * 			# Abstract method:
	 * 				-> method which does not have implimentation details/body
	 * 
	 * 			
	 * 		
	 * 
	 * 
	 * */
	
	
}
