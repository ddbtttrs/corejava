package javacollection;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class CollectionTest {

	public static void main(String[] args) {
		
		List<Integer> list = Arrays.asList(34,56,78,654,75,567,3,75,867,45,2432,645,45);
		List<String> lists = Arrays.asList("b34","a56","y78","t654","l75");
		Collections.sort(lists);
		System.out.println(lists);
		
		Collections.reverse(list);
		//System.out.println(list);
	}
	
	// WAP to print student list sort by Fname;
	// WAP to print student list sorted by age;
	// WAP to print employee list sorted by Fname and Salary
	
}
