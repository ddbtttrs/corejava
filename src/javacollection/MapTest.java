package javacollection;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class MapTest {

	public static void main(String[] args) {
		
		//Map<String, Integer> map = new HashMap<>();
		//Map<String, Integer> map = new TreeMap<>();
		Map<String, Integer> map = new LinkedHashMap<>();
		
		map.put("English", 80);
		map.put("Nepali", 90);
		map.put("Science", 88);
		map.put("Computer", 97);
		map.put("Math", 91);
		
//		for(String key: map.keySet()) {
//			
//			System.out.println(key+ " = "+ map.get(key));
//		}
		
		
		Map<String, List<Integer> > mlist = new LinkedHashMap<>();
		mlist.put("Math", List.of(55,66,88));
		mlist.put("Nepali", List.of(85,96,48));
		mlist.put("Computer", List.of(77,88,99));
		
		int mtotal = 0;
		int ntotal = 0;
		int ctotal = 0;
		for(String key : mlist.keySet()) {
			//System.out.println(key+ " = "+ mlist.get(key));
			
				for(int x : mlist.get("Math")) {
					mtotal += x;
					}
				
				for(int x : mlist.get("Nepali")) {
						ntotal += x;
					}
					
				for(int x : mlist.get("Computer")) {
							ctotal += x;
					}
				System.out.println("Math Total:	" + mtotal);
				System.out.println("Nepali Total:	" + ntotal);
				System.out.println("Computer Total: " + ctotal);
			}
		}
	}

