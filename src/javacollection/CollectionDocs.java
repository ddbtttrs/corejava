package javacollection;

public class CollectionDocs {

	/*
	 * --------------- Java Collection/Collection Framework--------------
	 * 
	 *  # Used to store and process dynamic multiple data/objects in java.
	 *  
	 *  1> List
	 *  	a> ArrayList
	 *  	b> LinkedList
	 *  
	 *  2> Set	:	set is defined as a collection of distinct, well-defined objects forming a group
	 *  	a> HashSet		:	Unique data only
	 *  	b> TreeSet		:	unique + sorted data
	 *  	c> LinkedList	:	unique + insertion in order
	 *  
	 *  
	 *  3> Map	:	Data managed in key-value pair
 * 		a> HashMap			:	<k,v> pair
 * 		b> TreeMap			:	<k,v> + sorted data
 * 		c> LinkedHashMap	: 	<k,v> + insertion in order
	 * */
	
}
