package javacollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ListTest {

	public static void main(String[] args) {
		
		//List<String> list = new ArrayList<>();
		List<String> list = new LinkedList<>();	
		
		list.add("Hari");
		list.add("Ram");
		list.add("Krishna");
		list.add("Shiva");
		list.add("Radha");
		
		//list.remove(2);
		//list.remove("Ram");
		//System.out.println(list.contains("Goma"));
		
		Collections.sort(list);
		for(String s : list) {
			System.out.println(s);
		}
		
		
		List<Book> bl = new LinkedList<>();
		
		for(int i=0; i<4; i++) {
			 Book b = new Book();
			 b.setName("Harry Poter");
			 b.setAuthor("Sarthak Bista");
			 b.setPrice(2300);
			 b.setGenre("Thriller");
			 bl.add(b);
		}
		 
		for(Book b : bl) {
			//System.out.println(b);
		
		
		}
	}
	
}
