package corejava;

import java.util.Scanner;

public class CircleArea {
	
	private static Scanner userIn;	
	
	public static void main(String[] args) {
		userIn = new Scanner(System.in);
		System.out.println("Enter you Radius: ");
		int rad = userIn.nextInt();
		double area =  Math.PI * Math.pow(rad,2);
		System.out.println("Area of Circle: " + area);
	};
	
	
}
