package ocalessons;

import java.util.Scanner;

public class Example1 {
	
	public static void main(String[] args) {
		 
		Scanner sc = new Scanner(System.in);
		
		System.out.println("input value for X: ");
		int x = sc.nextInt();
		System.out.println("input value for Y: ");
		int y = sc.nextInt();
		
		
		int result = x < y  ? 34 : 399;
		System.out.println(result);
	}
}
